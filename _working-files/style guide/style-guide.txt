Imports
=======
Un répertoire commun (all_sites)
Un répertoire par site
Utilisation des préfixes
    _b-     base (variables, reset, typo, forms…)
    _l-     layout (grands blocs type header, footer…)
    _m-     modules
    _p-     pages speciales


Globales
========
Un fichier par site **ne devrait contenir aucune règle**.
Fichiers spéciaux (ie, print).


Conventions d'écriture
======================
Utilisation de CSSBeautify et de CSSComb (à passer dans cet ordre).
Fichiers de préférences user joints dans ce répertoire.

Objectif :
  - remplacement des tabulations par deux espaces
  - blocs de déclarations scss séparés par une ligne vide
  - ordonnancement des propriétés par type