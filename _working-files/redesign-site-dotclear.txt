*** Redesign site dotclear.org ***

# Préface : pourquoi ?
Raisons rédactionnelles et ergonomiques (mises en valeur ou retraits)
Raisons esthétiques (moderniser)
Raisons éthiques et techniques (accessibilité, adaptabilité aux mobiles…)

# 1e partie : alors l'interface comment tu la veux ?
Portail ou zen ?
Ranger / classer
portail puis zen puis ni l'un ni l'autre


# 2e partie : les gabarits

Koz. 26 juillet 2013. 
Le bureau a validé les maquettes. Il faut maintenant que j'en fasse les déclinaisons pour tous les contextes du blog et selon les différentes résolutions.
Pour décider des seuils je me base non pas sur les tailles d'écran de tel ou tel support mais des points de rupture logiques de la maquette "grand format".

Fr. 26 juillet 2013
La structure du site est basée sur dotclear (évidemment), mais avec une particularité sur la page d'accueil : elle a un gabarit bien à elle qui fait appel à des includes (elle appelle des fichiers externes) pour insérer des contenus selon les différentes parties de la maquette.
Une autre particularité est que le header et le footer sont aussi inclus et sont présents sur tout le site (dont dev, dotaddict, wiki).

Koz. 27 juillet 2013.
À propos de la maquette du blog, Ève me fait remarquer qu'on pourrait croire que les titres des billets suivant le premier sont des intertitres du même billet. Trouver une séparation graphique nette (elle propose une ligne continue). Elle a raison, ça rend pas mal !
J'en profite pour procéder à quelques modifications sur la maquette du blog (à reporter si on en est satisfaits) : déplacement du titre à côté de la date, déplacement des infos de billet sous le billet. Sur le premier billet, ajout d'un lien "Commenter" spécifique qui pointera directement sur le formulaire et d'un petit bandeau regroupant les icônes de signalement aux réseaux sociaux.
Dernière modification du jour, comme suggéré à la réunion d'hier soir je remplace l'icône d'accueil Coquette par une icône monochrome.
Si toutes ces modifs reçoivent l'approbation des copains, je les reporterai partout où ce sera nécessaire.

Fr. 12 août 2013
Je ne me souviens plus si on a déterminé une largeur maxi souple (max-width) pour le site ou si on est sur du 940px (en regardant les fichiers AI, on dirait).

Koz. 13 août 2013. J'ai fait les maquettes sur du 960px mais je n'ai pas de religion là-dessus.

Fr. 14 août 2013. ça me va (dans mes habitudes aussi). Je fais le tour des remarques et options choisies pour la maquette, et propose une évolution prenant tout en compte (et quelques décorations en + si j'y arrive sur Illustrator)

Fr. 21 septembre 2013. Ne pas oublier d'ajouter le lien direct vers l'inscription de la mailing-liste.
séparer header et section avec sous-menu blog (header > commun, section > blog) > done.


# 3e partie : l'intégration

Koz. 6 novembre 2013. Conventions :

- les noms de fichiers css sans préfixe sont appelés directement dans le head
- les noms de fichiers css préfixés avec un _underscore sont les fichers inclus dans all_sites.css
- les noms de fichier préfixés fyi sont purement informatifs
- le fichier all_sites.css comporte uniquement les règles communes (reset, formulaires, layout, typo, liens…) + celles du header et du footer ; les contenus propres à chacun des sites (aka tout ce qui est dans #wrapper) sont stylés dans leurs propres fichiers.
- le fichier demo_design.html : comme son nom l'indique (à continuer de mettre au point)

Je trouve qu'un truc qui serait pas mal serait de faire des répertoires au sein de style2013 avec les alias vers les fichiers css des sites.

Tomek 1er mai 2015 : on s'organise : 
- Koz. s'occupe de refactoriser sass, css et dossiers
- Mega s'occupe de pondre un joli menu/sous-menu version mobile qui poutre
- Franck lui s'occupe de basculer les layouts en mobile first

# 4e partie : tests

# 5e partie : mise en prod

----

Crédits :
* Icônes monochromes : http://icomoon.io/app/ (Creative Commons)
* Illustration de l'abc : 20 Krazy Kids de Havana Street http://www.havanastreet.com/free.htm
* Fonts : Arsenal par Andriy Shevchenko @ Font Squirrel http://www.fontsquirrel.com/fonts/arsenal http://www.ukrainian-type.com/fontarsenal/ & Source Sans Pro par Adobe

----

todo
- lien abc : http://abc.dotaddict.org/post/2009/03/18/Bienvenue-sur-labc-Dotclear
- créer une page du blog rézosociaux et abonnements avec des ancres pour pouvoir lier vers cette page depuis les icônes rondes quand il y a plusieurs possibilités (ex. RSS, Twitter, mail)
- à tester : menu de niveau 2 en 14px tout en CAP -> testé, nope.
- refaire les favicon : celui de l'admin et celui du site - voir pour faire avec realfavicongenerator ? http://realfavicongenerator.net/
- virer le tourniquet du logo -> done
