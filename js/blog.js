$(function() {
	$('#content-info').after($('#sidebar').addClass('sidebar'));
	$('#sidebar>div').hide();
	
	var list = $('<ul class="sidebar"></ul>');
	var item, link;
	$('#sidebar>div h2').each(function() {
		link = document.createElement('a');
		link.href = '#';
		link.target_element = $(this).parent().get(0);
		$(link).append($(this).text());
		item = document.createElement('li');
		$(item).append(link);
		
		$(link).click(function() {
			$(list).find('li>a').not(this).each(function() {
				$(this.target_element).slideUp(100);
			});
			$(this.target_element).slideToggle(100);
			return false;
		});
		
		list.append(item);
	});
	list.find('li:last').addClass('last');
	$('#sidebar').before(list);
	if ($('input#q').val() != '') {
		$('div#search').show();
	}
});