$(function() {
	$('dl.modules dd').hide();
	$('dl.modules dt').css('cursor','pointer').click(function() {
		$('dd:visible',$(this).parent()).not($(this).next()).hide();
		$(this).next().slideToggle(100,function() {
			$('img',this).reflect({height:0.1,opacity:0.5});
		});
	});
});