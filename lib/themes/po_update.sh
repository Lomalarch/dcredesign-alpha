#!/bin/sh

THEME="dotclear"
SERVICES="/var/www/control/authdb/xmlrpc.php"
DOTCLEAR="/var/www/share/dotclear"

LANGS="fr"

export LANG=C

XGETTEXT=xgettext
MSGMERGE=msgmerge

if [ ! -d ./dotclear ]; then
	echo "Not in Dotclear.org themes root"
fi

extract_strings()
{
	$XGETTEXT \
	-f- \
	--sort-by-file \
	-L PHP -k__ \
	--no-wrap \
	--foreign-user \
	"$@"
}

update_po()
{
	po_file=$1
	pot_file=$2
	po_dir=`dirname $1`
	po_tmp=$po_dir/tmp.po~
	
	if [ ! -d $po_dir ]; then
		mkdir $po_dir
	fi
	
	if [ ! -f $po_file ]; then
		cp $pot_file $po_file
		perl -pi -e "s|; charset=CHARSET|; charset=UTF-8|sgi;" $po_file $po_file
	fi
	
	$MSGMERGE --no-location --no-wrap -o $po_tmp $po_file $pot_file
	mv $po_tmp $po_file
}

# PO template file
echo "Building public PO template..."

echo '<?php' > $THEME/__html_tpl.php
find $THEME -name '*.html' -exec grep -o '{{tpl:lang [^}]*}}' {} \; | sed 's/{{tpl:lang \(.*\)}}$/__\("\1");/' | sort -u \
	>> $THEME/__html_tpl.php
echo '?>' >> $THEME/__html_tpl.php

(find $THEME -name '*.php' -print && echo $SERVICES) | \
	extract_strings \
	--package-name="Dotclear.org theme" \
	-o $THEME/locales/_pot/public.pot \
	-x $DOTCLEAR/locales/_pot/date.pot \
	-x $DOTCLEAR/locales/_pot/public.pot \
	-x $DOTCLEAR/locales/_pot/plugins.pot
	
rm -f $THEME/__html_tpl.php

# Update locales
for L in $LANGS; do
	if [ ! -d $THEME/locales/$L ]; then
		mkdir -p $THEME/locales/$L
	fi
	echo "Updating $L"
	update_po $THEME/locales/$L/public.po $THEME/locales/_pot/public.pot
done