<?php
$core->url->unregister('tags');
$core->url->register('index','index','^index$',array('dotclearURLs','index'));
$core->url->register('language','language','^language$',array('dotclearURLs','language'));
$core->url->register('auth','auth','^auth/?$',array('dotclearURLs','auth'));
$core->url->register('my','my','^my/$',array('dotclearURLs','my'));
$core->url->register('myApply','my/apply','^my/apply$',array('dotclearURLs','myApply'));
$core->url->register('myDelete','my/delete','^my/delete$',array('dotclearURLs','myDelete'));
$core->url->register('myRecover','my/recover','^my/recover$',array('dotclearURLs','myRecover'));

$core->addBehavior('publicBeforeContentFilter',array('dotclearBehaviors','publicBeforeContentFilter'));

$core->tpl->addBlock('AuthIf',array('dotclearTPL','AuthIf'));
$core->tpl->addValue('AuthValue',array('dotclearTPL','AuthValue'));
$core->tpl->addValue('AuthTermsBox',array('dotclearTPL','AuthTermsBox'));

$core->tpl->addValue('Affiliates',array('dotclearTPL','Affiliates'));
$core->tpl->addValue('SitesMap',array('dotclearTPL','SitesMap'));

$core->tpl->addValue('SelectedLang',array('dotclearTPL','SelectedLang'));


$__autoload['dcUserClient'] = dirname(__FILE__).'/classes/class.dcUserClient.php';

l10n::set(dirname(__FILE__).'/locales/'.$_lang.'/public');

class dotclearURLs extends dcUrlHandlers
{
	public static function index()
	{
		$_ctx =& $GLOBALS['_ctx'];
		$core =& $GLOBALS['core'];
		
		$_ctx->posts = $core->blog->getPosts(array(
			'post_type' => 'page',
			'post_url' => 'index'
		));
		
		self::serveDocument('index.html');
		exit;
	}
	
	public static function language()
	{
		if (!empty($_POST['lang']) && preg_match('/dotclear\.org$/',$_POST['lang']))
		{
			http::redirect('http://'.$_POST['lang'].'/');
			
		}
		
		if (!empty($_POST['guess']))
		{
			$dlang = http::getAcceptLanguage();
			header('Content-Type: text/plain');
			
			if ($dlang != $GLOBALS['core']->blog->settings->system->lang) {
				$GLOBALS['__l10n'] = array();
				l10n::set(dirname(__FILE__).'/locales/'.$dlang.'/public');
				echo __('Dotclear is also available in <a href="http://dotclear.org/">English</a>.');
			}
			exit;
		}
		
		self::p404();
	}
	
	public static function auth($args)
	{
		if (!isset($_POST['auth_l']) || !isset($_POST['auth_p']) || !isset($_POST['auth_r']))
		{
			http::head(405);
			header('Content-Type: text/plain');
			echo 'Method not allowed';
			exit;
		}
		$login = $_POST['auth_l'];
		$password = $_POST['auth_p'];
		$redir = $_POST['auth_r'];
		
		$bits = parse_url($redir);
		$valid_redir = true;
		if (empty($bits['host']) || empty($bits['scheme']) || !empty($bits['port'])) {
			$valid_redir = false;
		}
		
		if (!preg_match('/(^|\.)dotclear\.org$/',$bits['host'])) {
			$valid_redir = false;
		}
		
		if (!$valid_redir) {
			http::head(403);
			header('Content-Type: text/plain');
			echo 'Method not allowed';
			exit;
		}
		
		try {
			self::initUserClient();
			$GLOBALS['x_client']->login($login,$password);
			http::redirect($redir);
		} catch (Exception $e) {
			self::setAuthError($e);
			$GLOBALS['x_value']['login'] = $login;
			$GLOBALS['x_value']['redir'] = $redir;
			self::serveDocument('my/index.html','text/html',false,false);
			exit;
		}
	}
	
	public static function my($args)
	{
		self::initUserClient();
		global $x_client, $x_value;
		
		if (!empty($_GET['logout']) && $x_client->getLogin()) {
			$x_client->dropUserCookie();
			http::redirect('/my/');
		}
		
		if ($x_client->getLogin())
		{
			$x_value['login']     = $x_client->getLogin();
			$x_value['firstname'] = isset($_POST['user_firstname']) ? $_POST['user_firstname'] : $x_client->getInfo('firstname');
			$x_value['name']      = isset($_POST['user_name'])      ? $_POST['user_name']      : $x_client->getInfo('name');
			$x_value['email']     = isset($_POST['user_email'])     ? $_POST['user_email']     : $x_client->getInfo('email');
			$x_value['email_cf']  = isset($_POST['user_email_cf'])  ? $_POST['user_email_cf']  : $x_client->getInfo('email');
			$user_pwd             = isset($_POST['user_pwd'])       ? $_POST['user_pwd']       : '';
			$user_pwd_cf          = isset($_POST['user_pwd_cf'])    ? $_POST['user_pwd_cf']    : '';
			
			if (!empty($_POST['update']))
			{
				try
				{
					if ($x_value['email'] != $x_value['email_cf']) {
						throw new Exception(__("Email addresses don't match."));
					}
					
					if ($user_pwd != $user_pwd_cf) {
						throw new Exception(__("Passwords don't match."));
					}
					
					$x_client->query('authdb.updateUser',$x_client->getInfo('user_id'),
						$x_value['email'],$user_pwd,$x_value['name'],$x_value['firstname'],'');
					
					http::redirect('/my/?upd=1');
				}
				catch (Exception $e)
				{
					self::setAuthError($e);
				}
			}
		}
		
		if (!empty($_GET['upd'])) {
			self::setAuthMsg(__('Your account has been successfully updated.'));
		}
		
		if (!empty($_GET['del'])) {
			self::setAuthMsg(__('Your account has been successfully deleted. Hope to see you back soon...'));
		}
		
		self::serveDocument('my/index.html','text/html',false,false);
	}
	
	public static function myApply($args)
	{
		self::initUserClient();
		global $x_client, $x_value;
		
		if ($x_client->getLogin()) {
			self::p404();
		}
		
		$x_value['login']      = isset($_POST['user_login'])     ? $_POST['user_login']     : '';
		$x_value['firstname']  = isset($_POST['user_firstname']) ? $_POST['user_firstname'] : '';
		$x_value['name']       = isset($_POST['user_name'])      ? $_POST['user_name']      : '';
		$x_value['email']      = isset($_POST['user_email'])     ? $_POST['user_email']     : '';
		$x_value['email_cf']   = isset($_POST['user_email_cf'])  ? $_POST['user_email_cf']  : '';
		$user_pwd              = isset($_POST['user_pwd'])       ? $_POST['user_pwd']       : '';
		$user_pwd_cf           = isset($_POST['user_pwd_cf'])    ? $_POST['user_pwd_cf']    : '';
		$x_value['read_terms'] = !empty($_POST['read_terms']);
		
		if (!empty($_GET['cr'])) {
			self::setAuthMsg(__('Your account has been successfully created.'));
		}
		
		if (!empty($_POST['create']))
		{
			try
			{
				if (!$x_value['read_terms']) {
					throw new Exception(__('You must read and agree with terms of use.'));
				}
				
				if ($x_value['email'] != $x_value['email_cf']) {
					throw new Exception(__("Email addresses don't match."));
				}
				
				if ($user_pwd != $user_pwd_cf) {
					throw new Exception(__("Passwords don't match."));
				}
				
				$x_client->query('authdb.createUser',
					$x_value['login'],$x_value['email'],$user_pwd,
					$x_value['firstname'],$x_value['name'],'');
				
				http::redirect('/my/apply?cr=1');
			}
			catch (Exception $e)
			{
				self::setAuthError($e);
			}
		}
		
		self::serveDocument('my/apply.html','text/html',false,false);
	}
	
	public static function myDelete($args)
	{
		self::initUserClient();
		global $x_client, $x_value;
		
		if (!$x_client->getLogin()) {
			self::p404();
		}
		
		if (!empty($_POST['no'])) {
			http::redirect('/my/');
			exit;
		}
		
		if (!empty($_POST['yes']))
		{
			try
			{
				$user_id = $x_client->getInfo('user_id');
				$x_client->query('authdb.dropUser',$user_id);
				$x_client->dropUserCookie();
				http::redirect('/my/?del=1');
				exit;
			}
			catch (Exception $e)
			{
				self::setAuthError($e);
			}
		}
		
		self::serveDocument('my/delete.html','text/html',false,false);
	}
	
	public static function myRecover($args)
	{
		self::initUserClient();
		global $x_client, $x_value;
		
		if ($x_client->getLogin()) {
			self::p404();
		}
		
		if (isset($_POST['u_login']) && isset($_POST['u_email']))
		{
			try
			{
				$x_client->query('authdb.recoverPassword',$_POST['u_login'],$_POST['u_email'],
					'account@dotclear.org',
					
					__('Dotclear.org account password recovery'),
					
					__('You claimed your Dotclear.org password back.')."\n\n".
					__('Username: The username you choosed')."\n".
					__('Password:')." %s\n\n".
					sprintf(__("You can modify your information on:\n%s"),http::getHost().'/my/').
					"\n\n".
					__('Welcome back on Dotclear.org!')."\n\n".
					"-- \n".
					__('The Dotclear team')
				);
				http::redirect('/my/recover?req=1');
			}
			catch (Exception $e)
			{
				self::setAuthError($e);
				$GLOBALS['x_value']['login'] = $_POST['u_login'];
				$GLOBALS['x_value']['email'] = $_POST['u_email'];
			}
		}
		
		if (!empty($_GET['req'])) {
			self::setAuthMsg(__('Your password is in your mailbox.'));
		}
		
		self::serveDocument('my/recover.html','text/html',false,false);
	}
	
	protected static function initUserClient()
	{
		$_POST['_dummy'] = 1; # Dummy post value to deactivate static cache
		
		global $core, $x_client, $x_value;
		$x_client = new dcUserClient();
		$x_client->readUserCookie();
		
		$x_value = array(
			'err' => '',
			'msg' => '',
			'redir' => $core->blog->url.$core->url->getBase('my').'/',
			'login' => '',
			'firstname' => '',
			'name' => '',
			'email' => '',
			'email_cf' => '',
			'terms_check' => false
		);
	}
	
	protected static function setAuthError($e)
	{
		global $x_value;
		
		if ($e instanceof Exception) {
			$x_value['err'] = __($e->getMessage());
		} else {
			$x_value['err'] = __($e);
		}
	}
	
	protected static function setAuthMsg($m)
	{
		global $x_value;
		$x_value['msg'] = $m;
	}
}

class dotclearBehaviors
{
	public static function publicBeforeContentFilter($core,$tag,$args)
	{
		$str =& $args[0];
		
		# Dotclear version
		if ($tag == 'EntryContent')
		{
			$str = preg_replace('/%%DC2_VERSION%%/',$_SERVER['DC2_VERSION'],$str);
			
			if (!empty($args['cache_in'])) {
				exit;$str = '';
			}
		}
	}
}

class dotclearTPL
{
	public static function AuthIf($attr,$content)
	{
		$if = array();
		$operator = isset($attr['operator']) ? $this->getOperator($attr['operator']) : '&&';
		
		if (isset($attr['logged_in'])) {
			$sign = (boolean) $attr['logged_in'] ? '' : '!';
			$if[] = $sign.'($x_client->getLogin())';
		}
		
		if (isset($attr['has_error'])) {
			$sign = (boolean) $attr['has_error'] ? '' : '!';
			$if[] = $sign.'($x_value[\'err\'])';
		}
		
		if (isset($attr['has_msg'])) {
			$sign = (boolean) $attr['has_msg'] ? '' : '!';
			$if[] = $sign.'($x_value[\'msg\'])';
		}
		
		if (!empty($if)) {
			return '<?php if('.implode(' '.$operator.' ',$if).') : ?>'.$content.'<?php endif; ?>';
		} else {
			return $content;
		}
	}
	
	public static function AuthValue($attr)
	{
		$f = $GLOBALS['core']->tpl->getFilters($attr);
		return '<?php echo '.sprintf($f,'$x_value[\''.addslashes($attr['type']).'\']').'; ?>';
	}
	
	public static function AuthTermsBox($attr)
	{
		return '<?php dotclearTPL::AuthTermsBoxHelper(); ?>';
	}
	
	public static function AuthTermsBoxHelper()
	{
		global $x_value;
		echo
		form::checkbox('read_terms',1,$x_value['read_terms']).' <label for="read_terms">'.
		sprintf(__('I read and agree with %1$sterms of use%2$s'),
		'<a onclick="window.open(this.href); return false;" href="/terms">','</a>'
		).'</label>';
	}
	
	public static function Affiliates()
	{
		return '<?php dotclearTPL::AffiliatesHelper(); ?>';
	}
	
	public static function AffiliatesHelper()
	{
		$affiliates_images = array(
			'White Logo'			=> 'w-logo.png',
			'Orange Logo'			=> 'o-logo.png',
			'White Small'			=> 'w-140x30.png',
			'Blue Small'			=> 'b-140x30.png',
			'Orange Small'			=> 'o-140x30.png',
			'White 120x240'		=> 'w-en-120x240.png',
			'Blue 120x240'			=> 'b-en-120x240.png',
			'Orange 120x240'		=> 'o-en-120x240.png',
			'White French 120x240'	=> 'w-fr-120x240.png',
			'Blue French 120x240'	=> 'b-fr-120x240.png',
			'Orange French 120x240'	=> 'o-fr-120x240.png',
		);
		
		$img = '<img src="'.http::getHost().'/affiliates/%s" alt="Dotclear 2" />';
		$link = '<a href="'.http::getHost().'/">%s</a>';
		
		foreach ($affiliates_images as $name => $src)
		{
			$I = sprintf($img,$src);
			
			echo
			'<div class="affiliates-img">'.
			'<h3>'.$name.'</h3>'.
			$I."\n".
			'<textarea rows="2" cols="40">'.
			htmlspecialchars(sprintf($link,$I),ENT_QUOTES).
			"</textarea>\n".
			"</div>\n";
		}
	}
	
	public static function SitesMap($args)
	{
		return '<?php dotclearTPL::SitesMapHelper(); ?>';
	}
	
	public static function SitesMapHelper()
	{
		global $core;
		
		$rs = $core->blog->getPosts(array(
			'post_type' => 'page',
			'post_url' => 'include-sites-map'
		));
		
		if ($rs->isEmpty()) {
			return;
		}
		
		$host = $_SERVER['HTTP_HOST'];
		$file = dirname(__FILE__).'/../../sitesmaps/'.$host.'%s.html';
		
		$trac_html=
		'<?xml version="1.0" encoding="UTF-8" ?>'."\n".
		"<!DOCTYPE html\n".
		'PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"'."\n".
		'"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">'."\n".
		'<html xmlns="http://www.w3.org/1999/xhtml" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:py="http://genshi.edgewall.org/" py:strip="">'."\n".
		"%s\n".
		"</html>\n";
		
		if (!file_exists($file)) {
			$do_file = is_writable(dirname($file));
		} else {
			$do_file = strtotime($rs->post_upddt) > filemtime($file);
		}
		
		$c =	$rs->getContent();
		
		if ($do_file) {
			file_put_contents(sprintf($file,''),$c);
			if ($host == 'dotclear.org') {
				file_put_contents(sprintf($file,'-trac'),sprintf($trac_html,$c));
			}
		}
		
		echo $c;
	}
	
	public static function SelectedLang($args)
	{
		return
		'<?php if($_SERVER[\'SERVER_NAME\'] == \''.addslashes($args['test']).'\') { '.
		'echo \' selected="selected"\'; '.
		'} ?>';
	}
}
?>
