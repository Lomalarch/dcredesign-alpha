$(document).ready (function() {

    // -- Modif DOM du bloc à afficher/masquer
    $('.to_expand').attr('aria-hidden','true').hide();
    $('.to_expand').attr('id', function(numero_id) { 
        return 'controlled-' + numero_id; // pour matcher aria-controls
    });

    // -- Modif DOM du déclencheur
    $('.expander').each(function(){
        $target = $(this).next('.to_expand')[0].id; // on va chercher l'id de la réponse
        $button = $('<button>', {
            'class': 'collapse-trigger',
            'aria-expanded': 'false',
            'aria-controls': $target
        });   
        // -- Output du bouton
        $(this).wrapInner($button);      
    });

    // Fonction afficher/masquer et modifs dynamiques ARIA
    $('.collapse-trigger' ).click(function() {
        if ($(this).attr('aria-expanded') == 'false') {
            $(this).attr('aria-expanded',true).parent().next('.to_expand').removeAttr('aria-hidden').slideDown();
        } else {
            $(this).attr('aria-expanded',false).parent().next('.to_expand').slideUp().attr('aria-hidden','true');
        }
    });
});