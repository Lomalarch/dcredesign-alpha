#!/bin/sh

#/bin/cp ../*.png ./ && for i in *.png; do convert -quality 95 $i `basename $i .png`.jpg; done && rm -f *.png && mogrify -resize 930x -quality 85 -blur 0.4x0.4 -sharpen 0.3x0.3 *.jpg

shots=original/shots
thumbs=original/thumbs

dest_s=./s
dest_t=./t

if [ ! -d $shots ]; then
	echo "No shots folder"
	exit 1
fi

if [ ! -d $thumbs ]; then
	echo "No thumbs folder"
	exit 1
fi

/bin/rm -rf $dest_s && /bin/mkdir $dest_s
/bin/rm -rf $dest_t && /bin/mkdir $dest_t

/bin/cp $shots/*.png $dest_s/
for i in $dest_s/*.png; do
	convert -quality 95 $i $dest_s/`basename $i .png`.jpg
done
/bin/rm -f $dest_s/*.png
mogrify -quality 85 -resize 930x -blur 0.4x0.4 -sharpen 0.3x0.3 $dest_s/*.jpg


/bin/cp $thumbs/*.jpg $dest_t/
mogrify -quality 85 -blur 0.3x0.3 $dest_t/*.jpg
